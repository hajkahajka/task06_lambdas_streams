package main.java;

import java.util.stream.Stream;

public class Lambdas{
    public static void main(String[] args){
        InterfLambdas maxValue = (a, b, c) -> Stream.of(a,b,c)
                .reduce(Integer::max)
                .orElse(Integer.MIN_VALUE);

        InterfLambdas averValue = (a, b, c) -> (int)Stream.of(a,b,c)
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();

        System.out.println("Max value is "+maxValue.method(3,6,4)
                +"\nAverage value is "+averValue.method(3,6,9));

    }
}
